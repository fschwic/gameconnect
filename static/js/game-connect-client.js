var connectionRetryWaitTime = 3000;

function connectPlayer(room, name) {
  var GameConnect = {};
  GameConnect.myName = name;
  GameConnect.myRoom = room;
  GameConnect.ws = connect(myRoom, myName);

  ws.onmessage = function(event) {
    console.log(event.data);

    var data = JSON.parse(event.data);
    console.log(data);
    router(data);
  }

  ws.onclose = function(closeEvent) {
    //console.log(closeEvent);
    var msg = "The connection to GameConnect was closed ";
    if (closeEvent.wasClean) {
      msg += "clean ";
    } else {
      msg += "unclean ";
    }
    msg += "because of \"" + closeEvent.reason + "\" ";
    msg += "with error code " + closeEvent.code + ".";
    console.log(msg);

    setTimeout(function() {
      console.log("Reconnecting " + myName);
      ws = null;
      connectPlayer(myRoom, myName);
    }, connectionRetryWaitTime);
  }

  ws.onerror = function(event) {
    console.log("ws error:");
    console.log(event);
  }

  function connect(room, name) {
    var ws = new WebSocket("ws://localhost:8081");
    ws.onopen = function(event) {
      var message = {};
      message.name = name;
      message.room = room;
      ws.send(JSON.stringify(message));
    };
    return ws;
  }

  function router(message){
    if(message.info){
      console.log("GameConnect INFO: " + message.info);
    }
    else {
      console.log("Unknown message: ");
      console.log(message);
    }
  }

  return GameConnect;
}
