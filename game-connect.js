var fs = require('fs');
var DECKS_FILE = './decks-sorted.json';

function getDeck(key){
  var decks = JSON.parse(fs.readFileSync(DECKS_FILE, 'utf8'));
  return decks[key];
}

function shuffle(randomstring, elements) {
  function getRandomInt() {
    return Math.floor(Math.random() * Math.floor(elements.length));
  }

  var shuffled_elements = [];
  for (var i = 0; i < elements.length; i++) {
    var extra = randomstring.charCodeAt(i % randomstring.length);
    var new_position = (extra + getRandomInt()) % elements.length;

    while (shuffled_elements[new_position]) {
      new_position = (new_position + 1) % elements.length;
    }
    shuffled_elements[new_position] = elements[i];
  }

  return shuffled_elements;
}

exports.deal = function deal(deck_key, player_count, randomstring) {
  if (!randomstring) {
    // TODO find a random source!?
    randomstring = "askdfhjkadsjhf";
  }

  var hands = [];
  for (var i = 0; i < player_count; i++) {
    hands.push([]);
  }

  _deal(randomstring, hands, getDeck(deck_key).deck);

  hands.forEach( hand => sortDeck(hand,deck_key) );
  return hands;
}

function sortDeck(hand, deck_key){
  var deckPositionIndex = getDeck(deck_key).deck.reduce((acc, cur, i) => { acc[cur] = i; return acc; }, {});

  hand.sort(function(a, b){
    return deckPositionIndex[a] - deckPositionIndex[b];
  });
}

function _deal(randomstring, buckets, deck) {
  var shuffled_deck = shuffle(randomstring, deck);

  for (var i = 0; i < shuffled_deck.length; i++) {
    buckets[i % buckets.length].push(shuffled_deck[i]);
  }

  return buckets;
}
