var http = require("http");
var express = require('express');
var handlebars = require('express-handlebars');
var bodyParser = require('body-parser');
var GameConnect = require('./game-connect.js');

/*
 * Config
 */
var port = process.env.PORT || 8081;
var host = process.env.HOST || "0.0.0.0";
var accessHostPort;

function sendCorsHeaders(req, res, next) {
  var origin = '*';
  if (req.headers.origin) {
    origin = req.headers.origin;
  }
  res.header("Access-Control-Allow-Origin", origin);
  res.header("Access-Control-Allow-Methods", "GET,POST");
  res.header("Access-Control-Allow-Credentials", "true");

  next();
};


var app = express();
app.use(express.static('static'));
app.use(require('morgan')('combined'));
// Body parser to be used in routes
// var bodyUrlEncodedParser = bodyParser.urlencoded({
//   extended: true
// });
// var bodyRawParser = bodyParser.raw({
//   type: '*/*',
//   limit: '400mb'
// });
// var bodyJsonParser = bodyParser.json();

/* templating */
var hbs = handlebars.create({
  defaultLayout: 'main'
});
app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');

/*
 * CORS
 */
app.all('*', sendCorsHeaders);


/*
 * Routes
 */
app.get('/',
  function(req, res) {});

app.get('/:deck_key/:player_count/:randomstring',
  function(req, res, next) {
    var game = GameConnect.deal(req.params.deck_key, req.params.player_count, req.params.randomstring);
    res.json(game);
  });



/*
 * WebSocket Server
 */
var server = http.createServer(app);
WebSocket = require("ws");
var websocketServer = new WebSocket.Server({
  server
});

websocketServer.on('connection', (ws) => {
  //send feedback to the incoming connection
  ws.send('{ "message" : "ok, connected"}');

  //when a message is received
  ws.on('message', (msgString) => {
    var message = JSON.parse(msgString);
    console.log(message);

    websocketServer.clients.add(ws);
    ws.send('{ "message" : "Thanks for the message." }');

    //for each websocket client
    websocketServer
      .clients
      .forEach(client => {
        //send the client the current message
        client.send(msgString);
      });
  });
});

/*
 * to actually serv
 */
server.listen(port, host, function() {

  var host = server.address().address
  var port = server.address().port

  console.log('GameConnect is listening at http://%s:%s', host, port)

})
